# Transmission lines

Wave propagation on transmission lines; The lumped-element circuit model for a transmision line.

## Wave Propagation on a Transmission Line
The wave equations for $`V(z)`$ and $`I(z)`$:
```math
\frac{\mathrm{d}^2V(z)}{\mathrm{d}z^2}=\gamma^2 V(z),\\
\frac{\mathrm{d}^2V(z)}{\mathrm{d}z^2}=\gamma^2 V(z),
```
where
```math
\gamma= \alpha+i\beta=\sqrt{\left(R+i\omega L\right)\left(G+i\omega C\right)}
```
The characteristic impedance, $`Z_0`$ can be defined as
```math
Z_0=\frac{R+i\omega L}{\gamma}=\frac{R+i\omega L}{G+i\omega C}
```
to relate the voltage and current on the line as 
```math
\frac{V_0^+}{I_0^+}=Z_0=-\frac{V_0^-}{I_0^-}.
```
$`\gamma`$ is called the complex propagation constant of the transmission line, amd it consists of a real part $`\alpha`$, called the attenuation constant of the line with units of Np/m, and an imaginary part β, called the phase constant of the line with units of rad/m.
The wavelength on the line is $`\lambda=2\pi/\beta`$ and the phase velocity is $`v_p=\omega/\beta = \lambda f`$.

## Definitions
1. Reflection coefficient at the load ($`l=0`$):
```math
\Gamma(l=0) = \frac{Z_L-Z_0}{Z_L+Z_0}
```
2. Reflection coefficient at a point $`l`$ along the line:
```math
\Gamma(l) = \Gamma(0)e^{-2i \beta l}
```
3. Time-average power flow along the line at the point z:
```math
P_{\text{avg}}=\frac{1}{2}\text{Re}\{ V(z)I^*(z) \}=\frac{1}{2}\frac{ \|V_0^+ \|^2 }{Z_0} \left( 1-\| \Gamma \|^2\right).
```
$`P_{\text{avg}}`$ is constant on the line (independt of $`l`$).

4. Return loss (RL)
```math
\text{RL}=-20\log\|\Gamma\| \text{dB}
```

- Matched load ($`\Gamma=0`$) -> RL=∞
- Total reflection ($`\Gamma=1`$) -> RL=0

5. Standing wave ratio (SWR)
```math
\text{SWR} = \frac{1+\|\Gamma\|}{1-\|\Gamma\|}
```
$`\text{SWR}=1`$ implies a matched load.

6. Input impedance seen looking toward the load at a distance $`z=-l`$
```math
Z_{\text{in}}=\frac{1+\Gamma e^{-2i \beta l}}{1-\Gamma e^{-2i \beta l}}Z_0=\frac{Z_L+i Z_0 \tan \beta l}{Z_0+i Z_L \tan \beta l}Z_0
```

## A note about circuitJS
You can add a transmission as a component in a CircuitJS simulation. Editing it gives only two parameters: Delay (in seconds) and impedance (in ohmes). I.e., it is a losless transmision line. Find the relation between the time delay to the propagation constant or to the length of the line.

