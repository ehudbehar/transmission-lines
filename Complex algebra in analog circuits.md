## Defs
```math
f(x)=\frac{1}{\pi}\left[ \int_0^\infty A(k)\cos k x \ \mathrm{d}k + \int_0^\infty B(k)\sin k x \ \mathrm{d}k \right]
```
provided that
```math
A(k)=\int_{-\infty}^{\infty}f(x)\cos kx\ \mathrm{d}x,\ \ B(k)=\int_{-\infty}^{\infty}f(x)\sin kx\ \mathrm{d}x.
```
Note that
```math
\int_{-\infty}^{\infty}\cos kx \ \mathrm{d}k=2\pi\delta(x),\ \int_{-\infty}^{\infty}\sin kx \ \mathrm{d}k=0.
```
```math
\int_0^\infty \delta(x-x_0)f(x)=f(x_0)\Theta(x_0) \text{ if } x_0\in\mathbb{R}
```
```math
\int_{-\infty}^{\infty}\cos k_0x \cdot \cos k x\ \mathrm{d}x=\frac{1}{4}\int_{-\infty}^{\infty}
\left[ \mathrm{e}^{\mathrm{i}x(-k-k_0)}+\mathrm{e}^{\mathrm{i}x(k-k_0)}+\mathrm{e}^{\mathrm{i}x(-k+k_0)}+\mathrm{e}^{\mathrm{i}x(k+k_0)} 
\right]\mathrm{d}x = \frac{\pi}{2}\left( \delta\left( -k-k_0 \right) + \delta\left( k-k_0 \right) + \delta\left( -k+k_0 \right) + \delta\left(k+k_0 \right) \right)
```
```math
\int_{-\infty}^{\infty}\sin k_0x \cdot \sin k x\ \mathrm{d}x=\frac{1}{4}\int_{-\infty}^{\infty}
\left[ -\mathrm{e}^{\mathrm{i}x(-k-k_0)}+\mathrm{e}^{\mathrm{i}x(k-k_0)}+\mathrm{e}^{\mathrm{i}x(-k+k_0)}-\mathrm{e}^{\mathrm{i}x(k+k_0)} 
\right]\mathrm{d}x = \frac{\pi}{2}\left( -\delta\left( -k-k_0 \right) + \delta\left( k-k_0 \right) + \delta\left( -k+k_0 \right) - \delta\left(k+k_0 \right) \right)
```
```math
 \ \delta(x)=\frac{1}{2\pi}\int_{-\infty}^{\infty}\mathrm{e}^{-\mathrm{i}kx}\mathrm{d}k=\frac{1}{2\pi}\int_{-\infty}^{\infty}\mathrm{e}^{+\mathrm{i}kx}\mathrm{d}k.
```

---
---
## RC circuit
![alt text](https://i.ibb.co/PhL9fdn/analog.png "Purcell Figure 8.7")

Kirchhoff loop equation solution:
```math
-Q/C+IR=V_0 \cos\omega t \Rightarrow I(t)=I_0 (\cos \omega t+\phi)
```

where
```math
I_0=V_0 \frac{\omega C}{\sqrt{1+\left(R \omega C\right)^2}},\ \ \phi=\frac{1}{R \omega C}.
```

Solution in the $`\omega`$ domain:
```math
\tilde V= Z \tilde I \Rightarrow \tilde I=\frac{V_0}{\frac{1}{i \omega C}+R}=V_0 \frac{\omega C}{\sqrt{1+\left(R \omega C\right)^2}}e^{i \theta}
```
where
```math
\theta = \arctan \left( \frac{1}{R \omega C} \right).
```
For which we have
```math
A(\omega)=\frac{V_0 \omega_0 C \pi}{\sqrt{1+\left(R \omega C\right)^2}}\cdot \cos\phi \left( \delta(\omega+\omega_0) +\delta(\omega-\omega_0)\right)
\\
B(\omega)=\frac{V_0 \omega_0 C \pi}{\sqrt{1+\left(R \omega C\right)^2}}\cdot \left(-\sin\phi\right) \left( \delta(\omega+\omega_0) +\delta(\omega-\omega_0)\right)
```
```math
I(t)=\frac{V_0 \omega_0 C }{\sqrt{1+\left(R \omega C\right)^2}}=\cos\phi\cos\omega_0 t-\sin\phi\sin\omega_0 t=\cos(\omega_0 t+\phi)
```
To get back to the $`t`$ domain:
```math
V(t)=\frac{1}{\pi}
```
---

| Function $`f(x)`$ | Cosine coeff $`A(k)`$ | Sine Coeff $`B(k)`$ | $`k`$ domain representation - $`F(k)=A+iB`$ |
| ------ | ------ | ------ | ------ |
| $`\frac{\exp \left(-\frac{\left(x-x_0\right){}^2}{2 \sigma ^2}\right)}{\sqrt{2 \pi } \sigma }`$ | $`e^{-\frac{1}{2} k^2 \sigma ^2} \cos \left(k x_0\right)`$ | $`e^{-\frac{1}{2} k^2 \sigma ^2} \sin \left(k x_0\right) `$ | $`e^{-\frac{1}{2} k^2 \sigma ^2} e^{i k x_0}`$ |
| $`e^{-2 \pi k_0 \text{abs}(x)}`$ | $`\frac{4 \pi k_0}{k^2+4 \pi ^2 k_0^2}`$ | 0 | $`\frac{4 \pi k_0}{k^2+4 \pi ^2 k_0^2}`$ |
| $` \frac{1}{\pi}\frac{\frac{\Gamma}{2}}{\left(\frac{\Gamma }{2}\right)^2+x^2} `$ | $`e^{-\frac{1}{2} \Gamma  \sqrt{k^2}}`$ | 0 | $`e^{-\frac{1}{2} \Gamma  \sqrt{k^2}}`$ | 

So we have a real valued function in each "axis" (The $`\Re`$ axis and $`\Im`$ axis). How to plot it $`\mathbb{C}`$? Superpositioning the two curves?
